/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.drogaria.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author heitorap
 */

@Entity
public class Funcionario extends GenericDomain{
    @Column(length = 50, nullable = false)  
    private String carteiraTrabalho;
    @Column(length = 10, nullable = false)  
    private String dataAdmissao;
    @Column(length = 50, nullable = false)  
    private Pessoa pessoa;
    
    public String getCarteiraTrabalho(){
        return carteiraTrabalho;
    }
    public void setCarteiraTrabalho(String ct){
        this.carteiraTrabalho = ct;
    }
    
    public String getDataAdimissao(){
        return dataAdmissao;
    }
    public void setDataAdimissao(String da){
        this.dataAdmissao = da;
    }
    
    public Pessoa getPessoa(){
        return pessoa;
    }
    public void setPessoa(Pessoa pessoa){
        this.pessoa = pessoa;
    }
}
